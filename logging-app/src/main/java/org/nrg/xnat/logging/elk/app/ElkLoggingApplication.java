package org.nrg.xnat.logging.elk.app;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class ElkLoggingApplication {
    public static void main(final String[] args) {
        log.info("Now starting the XNAT ELK demo application with the args: {}", args.length == 0 ? "<none>" : StringUtils.join(args, ", "));
        SpringApplication.run(ElkLoggingApplication.class, args);
    }
}
