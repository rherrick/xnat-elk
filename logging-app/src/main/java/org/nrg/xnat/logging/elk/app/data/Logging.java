package org.nrg.xnat.logging.elk.app.data;

import org.apache.commons.lang3.RandomUtils;

public class Logging {
    public Logging() {
        _id = RandomUtils.nextLong(1, 1000000L);
        _statement = "This is a recording.";
    }

    public long getId() {
        return _id;
    }

    public void setId(final long id) {
        _id = id;
    }

    public String getStatement() {
        return _statement;
    }

    public void setStatement(final String statement) {
        _statement = statement;
    }

    @Override
    public String toString() {
        return _id + ": " + _statement;
    }

    private long   _id;
    private String _statement;
}
