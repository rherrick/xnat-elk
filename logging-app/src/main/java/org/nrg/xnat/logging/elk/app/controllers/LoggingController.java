package org.nrg.xnat.logging.elk.app.controllers;

import lombok.extern.slf4j.Slf4j;
import org.nrg.xnat.logging.elk.app.data.Logging;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Slf4j
@RequestMapping("/logging")
public class LoggingController {
    @GetMapping
    public String loggingForm(final Model model) {
        final Logging logging = new Logging();
        model.addAttribute("title", "Logging");
        model.addAttribute("logging", logging);
        log.info("Someone loaded the logging page. Sending there with a default logging object: {}", logging);
        return "logging";
    }

    @PostMapping
    public String loggingSubmit(@ModelAttribute final Logging logging, final Model model) {
        log.info("Sending user to logging page. Submitted info is: {}", logging);
        model.addAttribute("title", "Logging");
        model.addAttribute("logging", logging);
        return "logging";
    }
}
