# XNAT ELK Vagrant Project and Sample Logging Application #

This is the XNAT ELK Vagrant Project and Sample Logging Application. It consists of two parts:

* The XNAT ELK Vagrant project, which builds and provisions a Vagrant VM that hosts the ELK stack.
* The sample Logging Application, which is a simple Spring Boot application that logs entries to the ELK server.

More details on standing up the XNAT ELK VM and running the sample application will be provided later.

