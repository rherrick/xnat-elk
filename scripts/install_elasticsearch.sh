#!/bin/bash

# https://www.elastic.co/guide/en/elasticsearch/reference/current/deb.html

wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add -
apt-get -y install apt-transport-https
echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" | tee -a /etc/apt/sources.list.d/elastic-6.x.list
apt-get update
apt-get -y install elasticsearch logstash kibana

cp /etc/kibana/kibana.yml /etc/kibana/kibana.yml.bak
cp /vagrant/configs/kibana.yml /etc/kibana/kibana.yml
cp /vagrant/configs/logback-listener.conf /etc/logstash/conf.d

# This is necessary for Kibana to work properly.
sed -i 's/127.0.1.1/# 127.0.1.1/' /etc/hosts

systemctl daemon-reload
systemctl enable elasticsearch.service logstash.service kibana.service
systemctl start elasticsearch.service logstash.service kibana.service


