#!/bin/bash
sudo --shell DEBIAN_FRONTEND=noninteractive PWD=/root <<'AS_ROOT'
    cd /root
    sed --in-place 's/^mesg n$/tty --silent \&\& mesg n/g' /root/.profile
    apt-get --assume-yes update
    apt-get --assume-yes --option Dpkg::Options::="--force-confdef" --option Dpkg::Options::="--force-confold" full-upgrade
    apt-get --assume-yes autoremove
    apt-get --assume-yes install zip unzip vim git tmux openjdk-8-jdk software-properties-common python-pip
    pip install --upgrade pip httpie
    wget --quiet https://bitbucket.org/rherrick/vm-init-files/downloads/vm-init-files.zip
    unzip vm-init-files.zip
    ./setup.sh
    rm --force setup.sh vm-init-files.zip

    /vagrant/scripts/install_elasticsearch.sh

AS_ROOT

